[Tutorial](https://ptrdev.com/symfony-tutorial-for-beginners/)

create entity
```shell
php bin/console make:entity
```

make controller
```shell
symfony console make:controller MainController
```

populate schema
```shell
php bin/console doctrine:schema:create
```

load fixtures
```shell
php bin/console doctrine:fixtures:load
```

